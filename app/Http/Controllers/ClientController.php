<?php

namespace App\Http\Controllers;
use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        // Retrieve clients with pagination
        $clients = Client::paginate(10); // This will enable pagination

        // Return the view with paginated clients
        return view('clients.index', compact('clients'));
    }

  public function store(Request $request)
    {
        // Create the client
        $client = Client::create($request->all());

        // Set a flash message
        session()->flash('success', 'Client added successfully');

        // Redirect back to the previous page
        return redirect()->back();
    }

    public function getClientData($id)
    {
        $client = Client::findOrFail($id);
        return response()->json(['client' => $client]);
    }
    

public function update(Request $request, $id)
{
    $client = Client::findOrFail($id);

    // Validation can be added here

    $client->update($request->all());

    session()->flash('success', 'Client updated successfully');

    return redirect()->route('clients.index');
}


}