<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $fillable = ['company_name', 'company_email', 'client_type', 'date_of_birth', 'company_id_number', 'contact_name', 'contact_email'];
}
