<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    protected $model = Client::class;

    public function definition()
    {
        return [
            'company_name' => $this->faker->company,
            'company_email' => $this->faker->companyEmail,
            'client_type' => $this->faker->randomElement(['person', 'company']),
            'date_of_birth' => $this->faker->date(),
            'company_id_number' => $this->faker->randomNumber(),
            'contact_name' => $this->faker->name,
            'contact_email' => $this->faker->safeEmail,
        ];
    }
}
