<div class=" mx-8 flex flex-col" x-data="{ openModal: false, clientType: 'person', client: null }">
    <table class=" text-center table-auto">
        <thead class="text-center">
            <tr>
                <th
                    class="text-center border-b-2 border-gray-200 bg-gray-100 text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    ID
                </th>
                <th
                    class="border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Company Name
                </th>
                <th
                    class="border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Company Email
                </th>

                <th
                    class="border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Client Type
                </th>

                <th
                    class="border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Client DOB
                </th>

                <th
                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Company Reg No
                </th>

                <th
                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Contact Name
                </th>

                <th
                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Contact Email
                </th>
                <!-- Add other relevant column headers -->
            </tr>
        </thead>
        <tbody class="text-center">
            @foreach ($clients as $client)
                <tr>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm" id="client-row-{{ $client->id }}">
                        {{ $client->id ?? '-' }}
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm text-blue-300 cursor-pointer">
                        <a href="#" x-on:click.prevent="editClient({{ $client->id }}), openModal = true">
                            {{ $client->company_name ?? '-' }}
                        </a>
                    </td>

                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {{ $client->company_email ?? '-' }}
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {{ $client->client_type ?? '-' }}
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {{ $client->date_of_birth ?? '-' }}
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {{ $client->company_id_number ?? '-' }}
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {{ $client->contact_name ?? '-' }}
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        {{ $client->contact_email ?? '-' }}
                    </td>
                    <!-- Add other relevant data cells with the same logic -->
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="mt-4">
        {{ $clients->links() }}
    </div>
    <!-- Modal -->
    <div x-show="openModal" class="fixed inset-0 z-10 overflow-y-auto" aria-labelledby="modal-title" role="dialog"
        aria-modal="true">
        <div class="flex items-end justify-center align-middle  pt-4 px-4 pb-20 text-center sm:p-0">
            <!-- Modal Backdrop -->
            <div class="fixed inset-0 bg-black bg-opacity-40 transition-opacity"></div>

            <!-- Modal Content -->
            <div
                class="align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                <form action="{{ route('clients.store') }}" method="POST">
                    <div class="px-4 py-5 bg-white sm:p-6">
                        <h3 class="text-lg leading-6 font-medium text-gray-900">Edit Client</h3>
                        <div class="mt-5">
                            <x-label for="company_name" :value="__('Company Name')" name="company_name" />
                            <x-input id="company_name" class="block mt-1 w-full" type="text" name="company_name"
                                x-model="client.company_name" required />

                            <x-label for="company_email" :value="__('Company Email')" class="mt-2" />
                            <x-input id="company_email" class="block mt-1 w-full" type="text" name="company_email"
                                x-model="client.company_email" required />



                            <x-label :for="'client_type'" :value="__('Client Type')" class="mt-2" />
                            <select id="client_type" name="client_type" x-model="clientType" class="block mt-1 w-full"
                                @change="console.log(clientType)">
                                <option value="person">Person</option>
                                <option value="company">Company</option>
                            </select>


                            <div x-show="clientType === 'person'">
                                <x-label for="date_of_birth" :value="__('Date of Birth')" class="mt-2" />
                                <x-input id="date_of_birth" class="block mt-1 w-full" type="date"
                                    name="date_of_birth" />
                            </div>

                            <div x-show="clientType === 'company'">
                                <x-label for="company_id_number" :value="__('Company ID Number')" class="mt-2" />
                                <x-input id="company_id_number" class="block mt-1 w-full" type="text"
                                    name="company_id_number" />
                            </div>

                            <x-label for="contact_name" :value="__('Contact Name')" class="mt-2" />
                            <x-input id="contact_name" class="block mt-1 w-full" type="text" name="contact_name"
                                required />

                            <x-label for="contact_email" :value="__('Contact Email')" class="mt-2" />
                            <x-input id="contact_email" class="block mt-1 w-full" type="text" name="contact_email"
                                required />

                        </div>
                    </div>
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <x-danger-button class="bg-blue-500 hover:bg-blue-700" @click="openModal = false"
                            type="button">
                            Close
                        </x-danger-button>
                        <x-button class="bg-blue-500 hover:bg-blue-700" type="submit">
                            Save
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- In your JavaScript -->
    <!-- In your JavaScript -->
    <script>
 


        function editClient(clientId) {
            // Make an AJAX request to fetch the client data by ID
            fetch(`/clients/${clientId}`, {
                    method: 'GET',
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                })
                .then(response => response.json())
                .then(data => {
                    // Handle the response data and set it in the modal
                    if (data.client) {
                        setClientData(JSON.stringify(data.client));
                        this.openModal = true; // Open the modal
                    }
                })
                .catch(error => {
                    // Handle errors
                    console.error(error);
                });
        }

        function setClientData(clientData) {

            console.log(this.client, "dsadsa")
            try {
                this.client = JSON.parse(clientData);
                console.log(this.client, "checking");
            } catch (error) {
                console.error(error);
            }
        }
    </script>



</div>
