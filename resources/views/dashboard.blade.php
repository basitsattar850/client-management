<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('Dashboard') }}</h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" x-data="{ openModal: false, clientType: 'person', client: null, setClientData(data) { this.client = data; } }">
            <div class="flex flex-row justify-between px-8">
                <h2 class="text-2xl font-extrabold">Clients</h2>
                <x-button @click="openModal = true">Add New</x-button>
            </div>
            
            <!-- Modal -->
            <div x-show="openModal" class="fixed inset-0 z-10 overflow-y-auto" aria-labelledby="modal-title"
                role="dialog" aria-modal="true">
                <div class="flex items-end justify-center align-middle  pt-4 px-4 pb-20 text-center sm:p-0">
                    <!-- Modal Backdrop -->
                    <div class="fixed inset-0 bg-black bg-opacity-40 transition-opacity"></div>

                    <!-- Modal Content -->
                    <div
                        class="align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                        <form action="{{ route('clients.store') }}" method="POST">
                            <div class="px-4 py-5 bg-white sm:p-6">
                                <h3 class="text-lg leading-6 font-medium text-gray-900">Add New Client</h3>
                                <div class="mt-5">
                                    <x-label for="company_name" :value="__('Company Name')" />
                                    <x-input id="company_name" class="block mt-1 w-full" type="text"
                                        name="company_name" required/>

                                    <x-label for="company_email" :value="__('Company Email')" class="mt-2" />
                                    <x-input id="company_email" class="block mt-1 w-full" type="text"
                                        name="company_email" required/>
                                    


                                        <x-label :for="'client_type'" :value="__('Client Type')" class="mt-2" />
                                        <select id="client_type" name="client_type" x-model="clientType" class="block mt-1 w-full"
                                            @change="console.log(clientType)">
                                            <option value="person">Person</option>
                                            <option value="company">Company</option>
                                        </select>


                                        <div x-show="clientType === 'person'">
                                            <x-label for="date_of_birth" :value="__('Date of Birth')" class="mt-2" />
                                            <x-input id="date_of_birth" class="block mt-1 w-full" type="date"
                                                name="date_of_birth" />
                                        </div>

                                        <div x-show="clientType === 'company'">
                                            <x-label for="company_id_number" :value="__('Company ID Number')" class="mt-2" />
                                            <x-input id="company_id_number" class="block mt-1 w-full" type="text"
                                                name="company_id_number" />
                                        </div>

                                        <x-label for="contact_name" :value="__('Contact Name')" class="mt-2" />
                                    <x-input id="contact_name" class="block mt-1 w-full" type="text"
                                        name="contact_name" required/>

                                        <x-label for="contact_email" :value="__('Contact Email')" class="mt-2" />
                                    <x-input id="contact_email" class="block mt-1 w-full" type="text"
                                        name="contact_email" required/>
                                    
                                </div>
                            </div>
                            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <x-danger-button class="bg-blue-500 hover:bg-blue-700" @click="openModal = false" type="button">
                                    Close
                                </x-danger-button>
                            <x-button class="bg-blue-500 hover:bg-blue-700" type="submit">
                                    Save
                                </x-button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @livewire('clients-table')
</x-app-layout>

<<script>
    function submitForm(event) {
        event.preventDefault();
    }
</script>
